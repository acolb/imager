<?php
	$message = ''; // For GUI
	
	if ($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_FILES['picture'])) {
		
		require 'Img.class.php';
		
		try {
			$Image = new Img($_FILES['picture']);
			
			$allowedFormats = ['png', 'jpg', 'jpeg'];
			$maxSize = 500 * 1000; // bytes
			$problems = $Image->problems($maxSize, $allowedFormats);
			
			if ($problems)
				throw new Exception($problems);
			
			if (!$Image->save('uploads'))
				throw new Exception("The server couldn't complete the upload.");
			
			// Make a short-named folder for the image
			// TODO: Feature - expand spectrum of names from 5 characters
			do {
				$quickDir = 'i/' . substr(uniqid(),-5);
			} while(file_exists($quickDir));
			mkdir($quickDir);
			
			// Move the image into the quick-linkable folder
			rename("$Image->saveDir/$Image->saveName", "$quickDir/$Image->saveName");
			
			// Convert the image to PNG (preserving the original)
			if (!imagepng(imagecreatefromstring(file_get_contents("$quickDir/$Image->saveName")), "$quickDir/image.png"))
				throw new Exception("The image could not be converted.");
			
			// Copy the folder index stereotype into place
			copy("stereotypes/index.php.stereotype", "$quickDir/index.php");
			
			$message = "<div class='alert alert-success'>\n<strong>Image uploaded!</strong><br><a href='$quickDir'>View it online.</a>\n</div>"; // Fallback
			
			header('Location: ' . $quickDir); // Redirect to dedicated image folder
			
		} catch (Exception $e) {
			$message = $e->getMessage();
			$message = "<div class='alert alert-warning'>\n<strong>The image couldn't be uploaded.</strong><br>$message\n</div>";
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<style>
	form {
		text-align:center;	
	}
	div.panel.panel-default {
		padding:0;
	}
	
	/* From www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3/*/
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
</style>
<title>Imager</title>

<?php require "header.php"; ?>

<div class="container">
	<?php echo $message; ?>
	<div class="row">
		<div class="panel panel-default col-sm-4 col-sm-offset-4">
			<div class="panel-heading">Upload a New Image</div>
			<div class="panel-body">
				<form method="post" enctype="multipart/form-data" id="fileUp">
					<div class="form-group">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<span class="btn btn-default btn-file btn-lg"><span><span class="glyphicon glyphicon-upload"></span> Choose file</span><input type="file" id="picture" name="picture"></span>
							
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	// Submit form on file select
	$("#picture").change(function () {
		$("#fileUp").submit();
	});
</script>
</body>
</html>