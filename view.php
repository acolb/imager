<?php
	/* Function to sanitize user input */
	function sanitize($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	/* AJAX response code */
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$commentsFile = fopen("comments.json", "a+"); // Open comments file, creating one if non-existent
		
		if (isset($_POST['poster']) and $_POST['poster'] != '' and isset($_POST['comment']) and $_POST['comment'] != '') {
			
			// Attempting to post a comment
			
			$poster = sanitize($_POST['poster']);
			$comment = sanitize($_POST['comment']);
			
			// Try getting previous comments
			
			$jsonString = fgets($commentsFile);
			try {
				$json = json_decode($jsonString);
				if (!$json or $json == '')
					throw new Exception("Faulty JSON");
			} catch (Exception $e) {
				$json = [];
			}
			
			// Add new comment to old JSON data
			array_push($json, [$poster, time(), $comment]);
			$jsonString = json_encode($json);
			
			fclose($commentsFile);
			
			// Overwrite the comments file with updated JSON
			file_put_contents("comments.json", $jsonString);
		}
		else {
			// Just read the comments
			$jsonString = fgets($commentsFile);
			fclose($commentsFile);
		}
		
		die($jsonString); // AJAX response, ignore the rest (HTML)
	}
?>
<!DOCTYPE html>
<html>
<head>
<style>
	img.showcase {
		max-width:100%;
		border:1px solid black;
		/* max-height:100vh; */
	}
	div.panel-body, div.panel-footer {
		text-align:center;
	}
	#shoutbox > .alert, form  {
		text-align:left;
	}
</style>
<script type="text/javascript" src="../../commenting.js"></script>
<title>Picture - Imager</title>
<?php
	if (!isset($directory))
		die("This should be run from /i/...../");

	require("header.php");
?>

<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<img src="image.png" class="showcase" alt="A user-provided image">
			<?php
				echo "<br><br><small>" . date ("F d Y H:i", filemtime("image.png")) . "</small>"; // Show file modified time
			?>
		</div>
		<div class="panel-footer">
			<a href="image.png" class="btn btn-info" role="button">
				Full-size <span class="glyphicon glyphicon-new-window"></span>
			</a>
			<button type="button" class="btn btn-info" data-toggle="modal" data-target="#linkable">Share Image</button>
		</div>
	</div>
	
	<div class="panel panel-default">
		<div class="panel-footer" id="shoutbox">
			Comments could not be loaded.
		</div>
		<div class="panel-footer">
			<form method="post" id="commenter">
				<h3>Comment on this image</h3><br>
				<input name="comment" id="comment" class="form-control" placeholder="Type it up here">
				<br>
				<div class="row">
					<div class="col-md-3">
						<input name="poster" id="poster" class="form-control" placeholder="Nickname">
					</div>
					<div class="col-md-9">
						<button type="submit" class="btn btn-success">Post Comment</button>
					</div>
				</div>
				
			</form>
		</div>
	</div>
</div>

<!-- Dialog for copying link -->
<div id="linkable" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Copy the link and share it with your friends</h4>
			</div>
			<div class="modal-body">
				<input class="form-control" value="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" onfocus="this.select()" readonly>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Done!</button>
			</div>
		</div>

	</div>
</div>

<script>
	$(document).ready(function () {
		
		// Fetch the comments and reset their submission form
		getComments("retrieve");
		$('#commenter')[0].reset();
		$("#commenter :input").prop("disabled", false);
		
		// Make the form submit into AJAX instead and then disable itself
		$("#commenter").submit(function (e) {
			if ($("#comment").val().trim().length < 3 || $("#poster").val().trim().length < 3) {
				$("#commenter h3").text("Please populate all fields")
			}
			else {
				getComments($("#commenter").serialize())
				$("#commenter :input").prop("disabled", true);
				$("#commenter h3").text("Comment posted!")
			}
			e.preventDefault();
		});
	});
</script>
</body>
</html>