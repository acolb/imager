/* Function to load AJAX-fetched comments into UI */
function comments(data) {
	var html = ""
	try {
		commentsList = JSON.parse(data).sort(function(a, b) {
			return b[1] - a[1];
		}); // Sort the comments, newest first
		
		for (var i=0; i < commentsList.length; i++) {
			var postedAt = new Date(commentsList[i][1] * 1000) 
			
			html += '<div class="alert alert-success">\n'
			html += "<strong>" + commentsList[i][2] + "</strong><br>"
			html += commentsList[i][0] + " on " + postedAt.toUTCString()
			html += '\n</div>\n'
		}
			
	} catch (e) {
		html = "No comments found.";
	}
	
	document.getElementById("shoutbox").innerHTML = html
}


/* Function to fetch comments via AJAX, while optionally posting a new one */
function getComments(data) {
	$.ajax({
		type: "POST",
		url: "",
		data: data,
		success: function(data){
			comments(data)
		},
		failure: function(errMsg) { 
			comments(false)
		},
	});
}