<?php
	/* 	
	A class for recently submitted images in a temporary location.
	Requires a file in order to be initialized, ie. new Img($_FILES['...'])
	*/
	class Img {
		
		private $file = false;
		
		public $saveDir = false;
		public $saveName = false;
		
		// Initialization function
		public function img($image) {
			$this->file = $image;
		}
		
		// Returns the original name of the uploaded image
		public function name() {
			return basename($this->file["name"]);
		}
		
		// Returns the temporary name of the uploaded image
		private function storedAs() {
			return $this->file["tmp_name"];
		}
		
		// Returns the file extension
		public function type() {
			return pathinfo($this->name(), PATHINFO_EXTENSION);
		}
		
		public function dimensions() {
			return getimagesize($this->storedAs());
		}
		
		// Returns the file size in bytes
		public function size() {
			return $this->file["size"];
		}
		
		// Checks for problems with the image, returns an error message string unless everything is fine (returning false)
		public function problems($maxBytes, $allowedFormats) {
			$errorMsg = '';
			
			if (!in_array($this->type(), $allowedFormats) or $this->dimensions() === false)
				$errorMsg .= "The file type or extension is unsupported. ";
			if ($maxBytes and $this->size() > $maxBytes)
				$errorMsg .= "The file is too large. ";
			
			if ($errorMsg != '')
				return $errorMsg;
			else return false;
		}
		
		// Saves the image into a requested, more permanent directory returning true if successful. Assumes that problems() has been called without errors.
		public function save($directory) {
			$directory = rtrim($directory, '/');
			if (!file_exists($directory))
				mkdir($directory);
			
			$this->saveDir = $directory;
			$this->saveName = uniqid('image', true) . '.' . $this->type(); // Gives the image an unique name
			
			return move_uploaded_file($this->storedAs(), $this->saveDir . '/' . $this->saveName);
		}
		
	}
?>