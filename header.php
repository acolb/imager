<?php
	define("IMAGER_BASEDIR", "http://" . $_SERVER['HTTP_HOST'] . "/imager");
	date_default_timezone_set('Europe/Helsinki');
?>
	<meta charset="utf-8">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#globalNav">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo IMAGER_BASEDIR; ?>">Imager</a>
		</div>
		<div class="collapse navbar-collapse" id="globalNav">
			<ul class="nav navbar-nav">
				<li>
					<a href="<?php echo IMAGER_BASEDIR; ?>">Upload!</a>
				</li>
			</ul>
		</div>
	</div>
</nav>